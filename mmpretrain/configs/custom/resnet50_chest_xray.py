_base_ = [
    '../_base_/models/resnet50.py',           # model settings
    '../_base_/datasets/imagenet_custom.py',    # data settings
    '../_base_/schedules/imagenet_bs256.py',  # schedule settings
    '../_base_/default_runtime.py',           # runtime settings
]

# model settings
model = dict(
    backbone=dict(
        frozen_stages=2,
        init_cfg=dict(
            type='Pretrained',
            checkpoint='https://download.openmmlab.com/mmclassification/v0/resnet/resnet50_8xb32_in1k_20210831-ea4938fc.pth',
            prefix='backbone',
        )),
        head=dict(
        num_classes = 2,
        topk = (1,)),
)

data_preprocessor = dict( 
    num_classes=2,
    mean = [0.485, 0.456, 0.406], 
    std=[0.229, 0.224, 0.225],
    to_rgb=False,
    )

train_dataloader = dict(
    batch_size = 32,
    dataset = dict(data_root='data/chest_xray/train'),
)

val_dataloader = dict(
    batch_size = 32,
    dataset = dict(data_root='data/chest_xray/val'),
)

test_dataloader = dict(
    batch_size = 32,
    dataset = dict(data_root='data/chest_xray/test'),
)

test_evaluator = [dict(type='Accuracy', topk=(1,)), dict(type='MultiLabelMetric', average='macro')]
val_evaluator = [dict(type='Accuracy', topk=(1,)), dict(type='MultiLabelMetric', average='macro')]

train_cfg = dict(by_epoch=True, max_epochs = 200, val_interval=1)

default_hooks = dict(
    checkpoint=dict(interval=5, type='CheckpointHook'),)

# If you want standard test, please manually configure the test dataset
#test_dataloader = val_dataloader
#test_evaluator = val_evaluator

# >>>>>>>>>>>>>>> Override schedule settings here >>>>>>>>>>>>>>>>>>>
# optimizer hyper-parameters
optim_wrapper = dict(
    optimizer=dict(type='SGD', lr=0.01, momentum=0.9, weight_decay=0.0001))
    #optimizer=dict(type='Adam', lr=0.01, weight_decay=0.001))
# learning policy
param_scheduler = dict(
    type='MultiStepLR', by_epoch=True, milestones=[15], gamma=0.1)
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

vis_backends=[dict(type='LocalVisBackend'),
              dict(type='TensorboardVisBackend')]

visualizer = dict( type='UniversalVisualizer', vis_backends=vis_backends )