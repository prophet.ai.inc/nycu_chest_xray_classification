

## 環境設定


```shell
conda create -n open-mmlab python=3.8 pytorch==1.10.1 torchvision==0.11.2 cudatoolkit=11.3 -c pytorch -y
conda activate open-mmlab
pip install openmim
git clone https://github.com/open-mmlab/mmpretrain.git
cd mmpretrain
mim install -e .
```

資料存放位址: mmpretrain/data

訓練 ResNet 18:

```shell
python tools/train.py configs/custom/resnet18_chest_xray.py
```

訓練 ResNet 50:

```shell
python tools/train.py configs/custom/resnet50_chest_xray.py
```

Inference ResNet 18 :

```shell
python tools/test.py configs/custom/resnet18_chest_xray.py CHECKPOINT.pth
```

Inference ResNet 50 :

```shell
python tools/test.py configs/custom/resnet50_chest_xray.py CHECKPOINT.pth
```
